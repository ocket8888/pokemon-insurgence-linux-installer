#!/usr/bin/env bash

# Error when any command fails. Prevents incomplete installs.
set -e

# Writes text in bold with a red foreground. Does **not** reset the ANSI
# settings afterwards. Prints to stderr.
#
# $1 - The string to print.
write_red_bold () {
	printf "\\033[00;31m\\033[1m%s\\033[0m\\n" "$1" >&2
}

# Writes text in bold. Does **not** reset the ANSI settings afterwards.
#
# $1 - The string to print.
write_bold () {
	printf "\\033[1m%b\\033[0m\\n" "$1"
}

# Prints the given step number.
#
# $1 - The step number/name
print_step () {
	printf "\\n\\033[31m\\033[1m##### OVERALL INSTALLATION STEP %s #####\\033[0m\\n" "$1"
}

# Prints usage information for this script.
usage () {
	echo "Usage: $0 [-f --force] [-h --help]"
	echo "	-f, --force    Force installation when the distribution cannot be"
	echo "	               detected, bypassing the installation of dependencies."
	echo "	-h, --help     Print usage information and exit."
}

write_bold "Unofficial Pokémon Insurgence Wine Installation Tool"
write_bold "Based on the Unofficial Pkmn Uranium Installer https://github.com/microbug/pokemon-uranium-on-macos"

INSTALL_DIR="$HOME/.pkmn_insurg"

if [ -d "$INSTALL_DIR" ]; then
	write_red_bold "Error: ~/pkmn_insurg already exists. If you have an incomplete installation, remove this directory first"
	write_red_bold "If you wish to update an older version to a newer version, use the update script".
	exit 1
fi

FORCE=false
if [ "$#" -gt 0 ]; then
	if [ "$1" == "-f" ] || [ "$1" == "--force" ]; then
		FORCE=true;
	elif [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
		usage;
		exit;
	else
		write_red_bold "Unrecognized option: $1";
		usage >&2;
		exit 1;
	fi
fi

print_step 2
write_bold "Detecting Distribution"

if ! [ -r /etc/lsb-release ] && [ "$FORCE" != "true" ]; then
	write_red_bold "Unsupported distribution. Use -f/--force (at your own risk) to try installing anyway.";
	exit 2;
fi

if [ "$FORCE" != "true" ]; then
	DISTRIBUTION="$(grep DISTRIB_ID /etc/lsb-release | cut -d = -f2)";
	write_bold "Distro detected as: ${DISTRIBUTION}";
	print_step 2
	write_bold "Checking for admin permissions"
	set +e;
	sudo true;
	if [ $? -ne 0 ]; then
		write_red_bold "This script needs administrative privileges to install packages"
		write_red_bold "To try installation without installing packages, use -f/--force"
		exit 3;
	fi
	set -e;
	print_step 3
	write_bold "Installing Wine and tools"
	case $DISTRIBUTION in
		"Pop"|"Ubuntu")
			sudo apt install wine winetricks wget;
			;;
		"Debian")
			sudo apt-get install wine winetricks wget;
			;;
		"Arch"|"Manjaro")
			sudo pacman -Syu wine winetricks wget;
			;;
		"Fedora")
			sudo dnf -y wine winetricks wget;
			;;
		*)
			write_red_bold "Unsupported distribution: ${DISTRIBUTION}"
			write_red_bold "To try anyway, use -f/--force"
			exit 3;
	esac

fi

print_step 4
write_bold "Creating virtual Windows installation at $INSTALL_DIR"
write_bold "Remember to accept all prompts to install Mono and/or Gecko, you may be asked several times"
write_bold "Lots of Wine logs (may look like nonsense) coming up..."
mkdir "$INSTALL_DIR"
export WINEPREFIX="$INSTALL_DIR"
export WINEARCH=win32
cd "$WINEPREFIX"
wineboot
wineserver -w --debug=0  # Wait for process to finish before continuing
winetricks -q directplay directmusic dsound d3dx9_43 renderer=gl win10 devenum dmsynth
sleep 5  # Let Wine finish spewing logs

print_step 5
write_bold "Adding game start script"
cat >"$INSTALL_DIR/run.sh" <<EOF
#!/usr/bin/env bash

# Exit on error
set -e

export WINEPREFIX="$INSTALL_DIR"

# By default we'd prefer the use of the primary wine path.
GAMEPATH="$INSTALL_DIR/drive_c/Program Files (x86)/Pokemon Insurgence/Game.exe"
if [ ! -f "$GAMEPATH" ]; then
    echo "The game files could not be found. This likely means your installation was incomplete."
    exit 1
fi

if [[ $# -eq 1 && $1 == "tail" ]] ; then
    wine "$GAMEPATH"
else
    wine "$GAMEPATH" &
    exit
fi
EOF
chmod +x "$INSTALL_DIR/run.sh"
if [ -d "$HOME/Desktop" ]; then
	write_bold "Creating shortcut on Desktop"
	ln -sf "$INSTALL_DIR/run.sh" "$HOME/Desktop/Run-Pokémon-Insurgence.desktop"
fi

print_step 6
write_bold "Clearing Wine caches"
rm -rf ~/.cache/wine ~/.cache/winetricks

print_step 7
write_bold "Installing Pokemon Insurgence"
wget -c "https://p-insurgence.com/releases/1.2.7/Pokemon Insurgence 1.2.7 Core.zip"
unzip "Pokemon Insurgence 1.2.7 Core.zip" -d "$INSTALL_DIR/drive_c/Program Files (x86)/"
mv -f "$INSTALL_DIR/drive_c/Program Files (x86)/Pokemon Insurgence 1.2.7 Core" "$INSTALL_DIR/drive_c/Program Files (x86)/Pokemon Insurgence"
mv -f "Pokemon Insurgence 1.2.7 Core.zip" "$INSTALL_DIR/drive_c/Program Files (x86)/"
echo "1.2.7" > "$INSTALL_DIR/version"

write_bold "Done!"
write_bold "Wait for all Wine configuration to finish (wait for any remaining windows to close), then REBOOT and check the guide on the Pokemon Insurgence Forums for next steps"
